var jQ=window.jQuery;
var joyWindow;

function jwindow_ajaxRequest(url,method,callback,datas,dataType)
{
	var cbk=callback;
	if(cbk==null||cbk=='')
	cbk=jWindow.defaultCallBack;

	   jQ.ajax({
	   url: url,
	   type:method,
	   cache:false,
	   data: datas,
	   dataType:dataType,
	   success: cbk.success,
	   error: cbk.failure
	}); 
}


function WindowConifg()
{
	this.width=700;
	this.height=400;
	this.alpha=-1;
}

var jWindow=joyWindow=({
		"windowId":"",
		"setWindowId":function(wid)
		{
			jWindow.windowId=wid;
		},
		"ajaxPost":function(url,callback,datas)
		{
			jwindow_ajaxRequest(url,"post",callback,datas+'&windowId='+jWindow.windowId,'text');
		},
		"ajaxGet":function(url,callback,datas)
		{
			jwindow_ajaxRequest(url,"get",callback,datas+'&windowId='+jWindow.windowId,'text');
		},
		"ajaxJsonPost":function(url,callback,datas){
			jwindow_ajaxRequest(url,"post",callback,datas+'&windowId='+jWindow.windowId,'json');
		},
		"ajaxJsonGet":function(url,callback,datas){
			jwindow_ajaxRequest(url,"get",callback,datas+'&windowId='+jWindow.windowId,'json');
		},
		"initWindow":function(config)
		{
			if(config instanceof WindowConifg)
			{
			 if(config.alpha>255)
				 config.alpha=255;
			 if(config.alpha>=0&&config.alpha<50)
				 config.alpha=50;
			 
			 var callback2={"success":function(text){}, "failure":function(text){alert('初始化出错，多实例模式的源服务器已关闭，程序无法继续运行!')}};
			 var datas="windowConfig.width="+config.width+"&windowConfig.height="+config.height+"&windowConfig.alpha="+config.alpha;
			 jwindow_ajaxRequest("/sys/sysutil/initWindow.action","post",callback2,datas,'text');
			}
		},
		"defaultCallBack":{
				"success": function(text)
				 {
				 try{
				   var jsonObj=eval(text);
					 alert(jsonObj.message);
				   }catch(e)
				   {
				   alert(text);
				   }
				 },
				 "failure":function(text)
				 {
					 if(window.confirm("请求出错!  是否查看返回信息?"))
						 alert(text);
				 }
			 }
});

