 function mask()
  {
	  jQ("#dialog-modal").dialog('open');
	  $('a.ui-dialog-titlebar-close').hide();
  }

  function unmask()
  {
	  $('a.ui-dialog-titlebar-close').show();
	  jQ("#dialog-modal").dialog('close');
  }
  /*
  function alertMsg(msg){
	  alert(msg);
  }*/
  
  function alertMsg(msg){
	  message_box.show(msg);
  }
  

function doSaveConfig(formId)
	{
	  
		   function handleSuccess(jsonObj) {
	//		unmask();
			   if(jsonObj.result){
				   message_box.success(jsonObj.message);
			   }else{
				   message_box.error(jsonObj.message);
			   }
		 }
		 
	   	function handleFailure(text){
		//	 unmask();
			 if(window.confirm("请求出错!是否查看返回信息?"))
				 	if(text.responseText=='')
				 		alertMsg("多实例模式的源服务器已停止，无法执行操作！");
					 	else
					 		alertMsg(text.responseText);
		 }


	  var jQCallBack={"success":handleSuccess,"failure":handleFailure}; 

	
		 var url='/sys/bindResource/saveBindResource.action';
		 var datas=jQuery("#"+formId).serialize();
		 //	 mask();
		 jWindow.ajaxJsonPost(url,jQCallBack,datas);
	}



 function doSaveConfigByDatapro(formId)
 {

	 function handleSuccess(jsonObj) {
		 //		unmask();
		 if(jsonObj.result){
			 message_box.success(jsonObj.message);
		 }else{
			 message_box.error(jsonObj.message);
		 }
	 }

	 function handleFailure(text){
		 //	 unmask();
		 if(window.confirm("请求出错!是否查看返回信息?"))
			 if(text.responseText=='')
				 alertMsg("多实例模式的源服务器已停止，无法执行操作！");
			 else
				 alertMsg(text.responseText);
	 }


	 var jQCallBack={"success":handleSuccess,"failure":handleFailure};


	 var url='/sys/bindResource/saveBindResource.action';
	 var datas=jQuery("#"+formId).serializeArray();
	 for(var i=0;i<datas.length;i++){
		 var name=datas[i].name;
		 if(name!='bindId' && name.substr(0,4)!='pro_'){
			 datas[i].name="pro_"+name;
		 }
	 }
	 var params=jQuery.param(datas);
	 jWindow.ajaxJsonPost(url,jQCallBack,params);
 }