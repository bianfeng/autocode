/*
	author:zhangshibin@shareyi.com
	date:20130630
	ver:J1.0.1
	
*/

(function(w){
	$.fn.extend({
		lazyClick : function(method,timeDiff,fail) {
			if(timeDiff==null||timeDiff==''){
				timeDiff=1000;
			}
			$(this).click(function(){
				var _this=$(this);
				var lastClick=_this.attr('lastClick');
				if(lastClick==undefined || lastClick==null && lastClick==''){
					lastClick=0;
				}
				var nowTime=(new Date()).getTime();
				if((nowTime-lastClick)>timeDiff){
					_this.attr('lastClick',nowTime);
					method.apply(this);
				}else{
					if(fail){
						fail.apply(this);
					}
				}
			});
		},
		/*文件选择器
		 *	@param dialogType save,open,directory
			@param fileType img,audio,vedio,doc,all,other
			@param fileExt	 文件后缀
		 * */
		fileChooser:function(sets){
			$(this).click(function(){
                var _set = $.extend({
                    dialogType : 'open',
                    fileType : 'all',
                    fileExt : ''
                },sets || {});

                var _self=$(this);
                if(_set['parentPath']==null){
                    var inputId=_self.attr("fileInput");
                    if(inputId==null || inputId ==''){
                        inputId=_self.data("inputid");
                    }
                    if(inputId!=null && inputId !=''){
                        var fileInput=$("#"+inputId).val();
                        if(fileInput!=null && fileInput!=''){
                            _set['parentPath']=fileInput;
                        }
                    }
                }

                $.ajax({
					   url: '/sys/fileutil/getFilePath.action',
					   type:'get',
					   cache:false,
					   data: _set,
					   dataType:'json',
					   success: function(json){
						   if(json.filePath&&json.filePath!=''){
							   $("#"+_self.attr('fileInput')).val(json.filePath);
						   }
					   },
					   error:function(text){
						   gDialog.fAlert('选择文件失败，原因是：'+text);
					   }
					});


			});
		},
        /*
        *文件打开器
        */
       fileOpener: function(sets){
               $(this).click(function(){
                   var _self=$(this);
                   var fileType=_self.data("filetype");
                   var inputId=_self.data("inputid");
                   var _set={"fileType":fileType,"inputId":inputId};
                   _set= $.extend( _set , sets||{});

                   if(_set.inputId!=null && _set.inputId!='') {
                       var _input = $("#" + _set.inputId);
                       var inputValue = _input.val();
                       if (inputValue != null && inputValue != '') {

                           if(inputValue.indexOf(' ')!=-1){
                               message_box.warning('文件路径中含有空格，请使用系统工具打开！');
                               return;
                           }

                           if("dir"==_set.fileType){
                               fileUtil.openDir(inputValue);
                           }
                           if("file"==_set.fileType){
                               fileUtil.editFile(inputValue);
                           }
                       }else{
                           message_box.error("打开失败，请先选择文件路径！");
                       }
                   }
               });
       }
	});
	
	
	
	window.fileUtil=({
		
		"startFile":function(editFilePath){
			$.ajax({
				   url: '/sys/fileutil/startFile.action',
				   type:'post',
				   cache:false,
				   data: {'editFilePath':editFilePath},
				   dataType:'json',
				   success: function(json){
                       if(json.result){
                           message_box.success(json.message);
                       }else{
                           message_box.error(json.message);
                       }
				   },
				   error:function(text){
					   gDialog.fAlert('操作失败，原因是：'+text);
				   }
				});
		},
		'editFile':function(editFilePath){
			 $.ajax({
				   url: '/sys/fileutil/editFile.action',
				   type:'post',
				   cache:false,
				   data: {'editFilePath':editFilePath},
				   dataType:'json',
				   success: function(json){
                       if(json.result){
                           message_box.success(json.message);
                       }else{
                           message_box.error(json.message);
                       }
				   },
				   error:function(text){
					   gDialog.fAlert('操作失败，原因是：'+text);
				   }
				}); 
		},
		'openDir':function(path){
			if(path==null&&$.trim(path)==''){
				gDialog.fAlert('目录为空!');
				return ;
			}
			
			 $.ajax({
				   url: '/sys/fileutil/openDir.action',
				   type:'post',
				   cache:false,
				   data: {'parentPath':path},
				   dataType:'json',
				   success: function(json){
					   if(json.result){
						   message_box.success(json.message);
					   }else{
                           message_box.error(json.message);
                       }
				   },
				   error:function(text){
					   gDialog.fAlert('操作失败，原因是：'+text);
				   }
				}); 
		},
		'openUrl':function(uri){
			if(uri==null&&$.trim(uri)==''){
				return ;
			}
			 $.ajax({
				   url: '/sys/sysutil/openUri.action',
				   type:'post',
				   cache:false,
				   data: {'uri':uri},
				   dataType:'json',
				   success: function(json){
                       if(json.result){
                           message_box.success(json.message);
                       }else{
                           message_box.error(json.message);
                       }
				   },
				   error:function(text){
					   gDialog.fAlert('操作失败，原因是：'+text);
				   }
				}); 
		},
		'readFileContent':function(filePath){
			if(filePath==null&&$.trim(filePath)==''){
				return '';
			}
			var content='';
			 $.ajax({
				   url: '/sys/fileutil/getFileContent.action',
				   type:'post',
				   cache:false,
				   data: {'editFilePath':filePath},
				   dataType:'json',
				   async: false, //非同步
				   success: function(json){
					   if(json.result){
						   content=json.message;
					   }else{
						   message_box.show(json.message,'error');
					   }
				   },
				   error:function(text){
					   gDialog.fAlert('读取文件内容操作失败，原因是：'+text);
				   }
				}); 
			 return content;
		},
		"saveFile":function(filePath,content){
			if(filePath==null&&$.trim(filePath)==''){
				return false;
			}
			var datas={'editFilePath':filePath,'editFileContent':content};
			 $.ajax({
				   url: '/sys/fileutil/saveFile.action',
				   type:'post',
				   cache:false,
				   data: datas,
				   dataType:'json',
				   async: false, //非异步
				   success: function(json){
					   if(json.result){
						  message_box.show('文件保存成功');
					   }else{
						   message_box.show(json.message,'error');
                           content=null;
					   }
				   },
				   error:function(text){
					   gDialog.fAlert('读取文件内容操作失败，原因是：'+text);
                       content=null;
				   }
				}); 
			 return content;
		},
		'listFiles':function(filePath,fileExt,callback){
			if(filePath==null&&$.trim(filePath)==''){
				return false;
			}
			var datas={'parentPath':filePath,'fileExt':fileExt};
			 $.ajax({
				   url: '/sys/fileutil/listFiles.action',
				   type:'post',
				   cache:false,
				   data: datas,
				   dataType:'json',
				   success:callback.success,
				   error:callback.error
				}); 
			
		}
	});
	
})(window);