<%
import groovy.sql.Sql;

//使用Sql对象查询
//第一个it是普通的对象应用，第二个it是groovy表达式
sql = Sql.newInstance("jdbc:mysql://192.168.147.35:3306/activiti_test", "root",
        "123456", "com.mysql.jdbc.Driver")
sql.execute("update test_id set id=id+1");
def row = sql.firstRow("select id from test_id");
print """ ${row.id}  """;

%>