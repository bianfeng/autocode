package com.shareyi.jshop.autocode.dbutil;

import java.util.List;

import com.shareyi.jshop.autocode.domain.Column;
import com.shareyi.jshop.autocode.domain.TableModel;


/**
 * 列处理工具类
 * @author david
 *
 */
public interface ColumnProcessor {

	
	/**
	 * 执行处理
	 * @param columns
	 */
	public void process(TableModel tableModel,List<Column> columns);
}
