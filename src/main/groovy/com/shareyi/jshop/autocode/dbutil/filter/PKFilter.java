package com.shareyi.jshop.autocode.dbutil.filter;

import java.util.ArrayList;
import java.util.List;

import com.shareyi.jshop.autocode.dbutil.ColumnFilter;
import com.shareyi.jshop.autocode.domain.Column;

public class PKFilter implements ColumnFilter {

	
	public List<Column> filterColumns(List<Column> columns) {
		List<Column> matchColumns=new ArrayList<Column>();
		for (Column column : columns) {
			if(isMatch(column)){
				matchColumns.add(column);
			}
		}
		return matchColumns;
	}

	public boolean isMatch(Column column) {
		return column.getIsPK() || "id".equals(column.getDataName());
	}

	

}
