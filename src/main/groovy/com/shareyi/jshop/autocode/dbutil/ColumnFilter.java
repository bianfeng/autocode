package com.shareyi.jshop.autocode.dbutil;

import java.util.List;

import com.shareyi.jshop.autocode.domain.Column;

public interface ColumnFilter {
		
	/**
	 * 获取符合规则的列
	 * @param columns
	 * @return
	 */
	public List<Column> filterColumns(List<Column> columns);
	

	/**
	 * 列是否符合过滤器
	 * @param column
	 * @return
	 */
	public boolean isMatch(Column column);
}
