package com.shareyi.jshop.autocode.domain;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.shareyi.jshop.autocode.tableutil.PubUtils;

public class TableModel {
	
	private TableDefine tableDefine;
	private String searchKeys;
	private List<OrderColumn> orderColumns;
	
	private String allColumn;
	private String queryList;
	private String addList;
	private String updateList;
	private String viewList;
	
	/**创建时间字段*/
	private String createTime;
	/**更新时间字段*/
	private String updateTime;

	private Map<String,List<String>> fieldListMap;
	
	public Map<String,Dict> dictMap;

	public TableModel() {
		this.fieldListMap =new HashMap<String, List<String>>();
	}
	
	
	public TableDefine getTableDefine() {
		return tableDefine;
	}
	public void setTableDefine(TableDefine tableDefine) {
		this.tableDefine = tableDefine;
	}
	public String getSearchKeys() {
		return searchKeys;
	}
	public void setSearchKeys(String searchKeys) {
		this.searchKeys = searchKeys;
	}
	
	public List<String> getSearchKeyList() {
		return PubUtils.stringToList(searchKeys);
	}
	
	
	public List<OrderColumn> getOrderColumns() {
		return orderColumns;
	}
	public void setOrderColumns(List<OrderColumn> orderColumns) {
		this.orderColumns = orderColumns;
	}
	public String getQueryList() {
		return queryList;
	}
	
	public void setQueryList(String queryList) {
		this.queryList = queryList;
	}
	public String getAddList() {
		return addList;
	}
	public void setAddList(String addList) {
		this.addList = addList;
	}
	public String getUpdateList() {
		return updateList;
	}
	public void setUpdateList(String updateList) {
		this.updateList = updateList;
	}
	public String getViewList() {
		return viewList;
	}
	public void setViewList(String viewList) {
		this.viewList = viewList;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	/**
	 * 是否不在List中
	 * @param listName
	 * @param columnName
	 * @return
	 */
	public boolean isNotInList(String listName,String columnName){
		return !isInList(listName, columnName);
	}
	
	/**
	 * 是否在List中
	 * @param listName
	 * @param columnName
	 * @return
	 */
	public boolean isInList(String listName,String columnName){
		List<String> columnNameList=this.getColumNameList(listName);
		//如果为空，默认为TRUE
		if(CollectionUtils.isEmpty(columnNameList)){
			return true;
		}
		return columnNameList.contains(columnName);
	}


	/**
	 * 根据listName获取list列表
	 * @param listName
	 * @return
	 */
	public List<String> getColumNameList(String listName){
		List<String> columnNameList=null;
		if(fieldListMap.containsKey(listName)){
			columnNameList= fieldListMap.get(listName);
		}else{
			columnNameList=this.parseListStrToListAndPutIntoMap(listName);
		}

		return columnNameList;
	}
	
	/**
	 * 将字符串转换为List<String>便于之后contains校验，也可以转为Set
	 * @param listName
	 */
	private List<String> parseListStrToListAndPutIntoMap(String listName) {
		String listStr=null;
		if("queryList".equals(listName)){
			listStr=queryList;
		}else if("addList".equals(listName)){
			listStr=addList;
		}else if("updateList".equals(listName)){
			listStr=updateList;
		}else if("viewList".equals(listName)){
			listStr=viewList;
		}else{
			listStr= allColumn;
		}
		if(StringUtils.isEmpty(listStr)){
			fieldListMap.put(listName, null);
			return null;
		}
		
		String[] columnNameArr=listStr.split(",");
		List<String> columnNameList=Arrays.asList(columnNameArr);
		fieldListMap.put(listName, columnNameList);
		return columnNameList;
		
	}

	
	/**
	 * 将字符串转换为List<String>便于之后contains校验，也可以转为Set
	 * @param listName
	 */
	public int listSize(String listName) {
		List<String> columnNameList=null;
		if(fieldListMap.containsKey(listName)){
			columnNameList= fieldListMap.get(listName);
		}else{
			columnNameList=this.parseListStrToListAndPutIntoMap(listName);
		}
		//如果为空，默认为全部
		if(CollectionUtils.isEmpty(columnNameList)){
			return tableDefine.getColumns().size();
		}
		
		return columnNameList.size();
		
	}



	/**
	 * 所有列表中是否包含tag
	 * @param tagName
	 * @return
	 */
	public boolean listContainsTag(String tagName){
		for(Column column:tableDefine.getColumns()){
			if(column.getJspTag().equalsIgnoreCase(tagName)){
				return true;
			}
		}
		return false;
	}


	/**
	 * 列表中是否包含tag
	 * @param listName
	 * @param tagName
	 * @return
	 */
	public boolean listContainsTag(String listName,String tagName){
		List<String> list=getColumNameList(listName);
		if(CollectionUtils.isEmpty(list)){
			return false;
		}

		for(Column column:tableDefine.getColumns()){
			if(list.contains(column.getColumnName())  && column.getJspTag().equalsIgnoreCase(tagName)){
				return true;
			}
		}
		return false;
	}

	public String getAllColumn() {
		return allColumn;
	}


	public void setAllColumn(String allColumn) {
		this.allColumn = allColumn;
	}


	public Map<String, Dict> getDictMap() {
		return dictMap;
	}


	public void setDictMap(Map<String, Dict> dictMap) {
		this.dictMap = dictMap;
	}

}
