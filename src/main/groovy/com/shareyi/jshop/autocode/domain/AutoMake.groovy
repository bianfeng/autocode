package com.shareyi.jshop.autocode.domain;

class AutoMake {
	
	String baseDir;
	String id;
	List<Template> templates;
	
	Map<String,String> props=new HashMap();
	
	public void addProp(String key,String value){
		props.put(key, value);
	}
	
	public String getProp(String key){
		return props.get(key);
	}


}
