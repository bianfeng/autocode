package com.shareyi.jshop.autocode.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.shareyi.jshop.autocode.dbutil.filter.PKFilter;
import com.shareyi.jshop.autocode.tableutil.TableNameUtil;

public class TableDefine {

	private String id;
	private String pageSize;
	private String cnname;
	private Boolean isPaged;
	private String dbTableName;
	Map<String,Column> columnMap;
	private List<Column> columns;
	private List<Column> pkColumns;
	

	
	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getCnname() {
		return cnname;
	}

	public void setCnname(String cnname) {
		this.cnname = cnname;
	}

	public Boolean getIsPaged() {
		return isPaged;
	}

	public void setIsPaged(Boolean isPaged) {
		this.isPaged = isPaged;
	}

	public String getDbTableName() {
		return dbTableName;
	}

	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}

	
	public Map<String, Column> getColumnMap() {
		if(columnMap==null){
			columnMap=new HashMap<String, Column>();
			for (Column column : columns) {
				columnMap.put(column.getColumnName(),column);
			}
		}
		return columnMap;
	}

	public void setColumnMap(Map<String, Column> columnMap) {
		this.columnMap = columnMap;
	}

	public Column getColumnByColumnName(String columnName)
	{
		return getColumnMap().get(columnName);
	}
	
	
	public List<Column> getPkColumns(){
		if(pkColumns==null){
			PKFilter pkFilter=new PKFilter();
			pkColumns=pkFilter.filterColumns(columns);
		}
		return pkColumns;
	}
	
	
	public Column getPkColumn(){
		List<Column> list=getPkColumns();
		if(CollectionUtils.isEmpty(list)){
			return null;
		}
		return list.get(0);
	}
	
	
	public String getVarDomainName(){
		return TableNameUtil.lowerCaseFirst(this.id);
	}
}
