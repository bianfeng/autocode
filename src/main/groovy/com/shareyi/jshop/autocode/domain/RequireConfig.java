package com.shareyi.jshop.autocode.domain;

/**
 *
 * 依赖资源的配置
 * Created by david on 2016/4/24.
 */
public class RequireConfig implements Comparable<RequireConfig>{

    /**
     * 字段列的标签，用于表示其展现形式
     */
    public String columnTag;
    /**
     * 字段标签对于的requireJs模块
     */
    public String requireModule;
    /**
     * 描述信息
     */
    public String desc;
    /**
     * 依赖的script或者css等
     */
    public String requireText;

    public int sortNum=0;

    public String getColumnTag() {
        return columnTag;
    }

    public void setColumnTag(String columnTag) {
        this.columnTag = columnTag;
    }

    public String getRequireModule() {
        return requireModule;
    }

    public void setRequireModule(String requireModule) {
        this.requireModule = requireModule;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRequireText() {
        return requireText;
    }

    public void setRequireText(String requireText) {
        this.requireText = requireText;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }



    @Override
    public int compareTo(RequireConfig o) {
        return this.sortNum-o.sortNum;
    }
}
