package com.shareyi.jshop.autocode.domain;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import java.util.*;

/**
 * requireJs依赖信息
 * Created by david on 2016/4/24.
 */
public class RequireInfo {

    public Set<String> columnTagSet;
    public List<RequireConfig> requireConfigList;
    public Map<String,List<Column>> columnListMap;
    public String listName;



    public RequireInfo(){
        columnTagSet=new HashSet<String>();
        requireConfigList =new ArrayList<RequireConfig>();
        columnListMap=new HashMap<String, List<Column>>();
    }

    /**
     * 是否含有依赖模块
     * @return
     */
    public boolean hasModule(){
        return CollectionUtils.isNotEmpty(requireConfigList);
    }


    /**
     * 是否含有依赖模块
     * @param moduleName
     * @return
     */
    public boolean containsModule(String moduleName){
        if(MapUtils.isNotEmpty(columnListMap)){
            return columnListMap.containsKey(moduleName);
        }
        return false;
    }

    /**
     * 是否含有依赖模块
     * @param columnTag
     * @return
     */
    public boolean containsTag(String columnTag){
        if(CollectionUtils.isNotEmpty(columnTagSet)){
            return columnTagSet.contains(columnTag);
        }
        return false;
    }

    /**
     * 聚合依赖模块  'jquery','xheditor'
     * @return
     */
    public String getModuleNames(String wrap){
        if(CollectionUtils.isEmpty(requireConfigList)){
            return "";
        }

        StringBuilder moduleInfo=new StringBuilder();
        Set<String> moduleSet=new HashSet<String>();
        for(int i=0;i< requireConfigList.size();i++){
            RequireConfig requireConfig=requireConfigList.get(i);
            if(moduleSet.contains(requireConfig.requireModule)){
             continue;
            }else{
                moduleSet.add(requireConfig.getRequireModule());
            }
            if(i>0){
                moduleInfo.append(",");
            }
            moduleInfo.append(wrap).append(requireConfig.getRequireModule()).append(wrap);
        }
        return moduleInfo.toString();
    }





    /**
     * 聚合依赖模块  'jquery','xheditor'
     * @return
     */
    public String getRequireText(){
        if(CollectionUtils.isEmpty(requireConfigList)){
            return "";
        }

        StringBuilder moduleInfo=new StringBuilder();
        Set<String> moduleSet=new HashSet<String>();
        for(int i=0;i< requireConfigList.size();i++){
            RequireConfig requireConfig=requireConfigList.get(i);
            if(moduleSet.contains(requireConfig.requireModule)){
                continue;
            }else{
                moduleSet.add(requireConfig.getRequireModule());
            }
            moduleInfo.append(requireConfig.getRequireText()).append("\n");
        }
        return moduleInfo.toString();
    }



    public List<Column> getColumnListByModule(String module){
        return columnListMap.get(module);
    }

    public List<RequireConfig> getRequireConfigList() {
        return requireConfigList;
    }

    public void setRequireConfigList(List<RequireConfig> requireConfigList) {
        this.requireConfigList = requireConfigList;
    }

    public Map<String, List<Column>> getColumnListMap() {
        return columnListMap;
    }

    public void setColumnListMap(Map<String, List<Column>> columnListMap) {
        this.columnListMap = columnListMap;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    /**
     * 添加列到模块中
     * @param column
     * @param requireConfig
     */
    public void addColumnToModule(Column column, RequireConfig requireConfig) {
        List<Column> columnList=null;
        if(columnListMap.containsKey(requireConfig.getRequireModule())){
            columnList=columnListMap.get(requireConfig.getRequireModule());
        }else {
            requireConfigList.add(requireConfig);
            columnList=new ArrayList<Column>();
            columnListMap.put(requireConfig.getRequireModule(),columnList);
        }
        columnTagSet.add(requireConfig.getColumnTag());
        columnList.add(column);
    }


    /**
     * 排序
     */
    public void sortModule() {
        if(CollectionUtils.isNotEmpty(requireConfigList)){
            Collections.sort(requireConfigList);
        }
    }

    public Set<String> getColumnTagSet() {
        return columnTagSet;
    }

    public void setColumnTagSet(Set<String> columnTagSet) {
        this.columnTagSet = columnTagSet;
    }
}
