package com.shareyi.jshop.autocode.domain;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * 自动生成代码需要的参数，调用时
 * Created by david on 2015/1/18.
 */
public class AutoCodeParams {

    public   String autoXmlPath;
    public  String tableModelPath;
    public  String baseDir;
    public String templateBaseDir;
    public Config config;
    public boolean overideFlag;
    public String[] templateIds;
    private HashSet<String> templateIdSet;


    public String getAutoXmlPath() {
        return autoXmlPath;
    }

    public void setAutoXmlPath(String autoXmlPath) {
        this.autoXmlPath = autoXmlPath;
    }

    public String getTableModelPath() {
        return tableModelPath;
    }

    public void setTableModelPath(String tableModelPath) {
        this.tableModelPath = tableModelPath;
    }

    public String getBaseDir() {
        return baseDir;
    }

    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
    }

    public String getTemplateBaseDir() {
        return templateBaseDir;
    }

    public void setTemplateBaseDir(String templateBaseDir) {
        this.templateBaseDir = templateBaseDir;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public boolean isOverideFlag() {
        return overideFlag;
    }

    public void setOverideFlag(boolean overideFlag) {
        this.overideFlag = overideFlag;
    }

    public String[] getTemplateIds() {
        return templateIds;
    }

    public void setTemplateIds(String[] templateIds) {
        this.templateIds = templateIds;
    }

    public boolean containsTemplateIds(String templateId){
        Set<String> set=this.getTemplateIdSet();
        return set.contains(templateId);
    }


    public Set<String> getTemplateIdSet(){
        if(templateIdSet==null){
            templateIdSet=new HashSet<String>();
            if(templateIds!=null) {
                for (String templateId : templateIds) {
                    templateIdSet.add(templateId);
                }
            }
        }
        return templateIdSet;
    }
}
