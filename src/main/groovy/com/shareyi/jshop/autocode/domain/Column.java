package com.shareyi.jshop.autocode.domain;

import java.io.Serializable;

public class Column  implements Serializable {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 121487859643283967L;
	private String dataName,columnName,jspTag,cnname,columnType,dictName,comment;
	 private Boolean canBeNull=false,readonly=false,isPK=false;
	 private Integer length;
	public String getDataName() {
		return dataName;
	}
	public void setDataName(String dataName) {
		this.dataName = dataName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public String getJspTag() {
		return jspTag;
	}
	public void setJspTag(String jspTag) {
		this.jspTag = jspTag;
	}
	public String getCnname() {
		return cnname;
	}
	public void setCnname(String cnname) {
		this.cnname = cnname;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public Boolean getCanBeNull() {
		return canBeNull;
	}
	public void setCanBeNull(Boolean canBeNull) {
		this.canBeNull = canBeNull;
	}
	public Boolean getReadonly() {
		return readonly;
	}
	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}
	
	public Boolean getIsPK() {
		return isPK;
	}
	public void setIsPK(Boolean isPK) {
		this.isPK = isPK;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	 
	public String getDictName() {
		return dictName;
	}
	public void setDictName(String dictName) {
		this.dictName = dictName;
	}
	public void setLength(String length) {
		
		if(length!=null){
			try{
				this.length=Integer.parseInt(length);	
			}catch (Exception e) {
			}
		}
		
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	 
}
