package com.shareyi.jshop.autocode.tableutil;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by david on 2016/4/24.
 */
public class AutoCodeStringUtils {


    /**
     * 将string转为list
     * @param str
     * @param sep
     * @return
     */
    public static List<String> strToList(String str,String sep){
        if(StringUtils.isEmpty(str)||StringUtils.isEmpty(sep)){
            return Collections.emptyList();
        }


        String[] strs=str.split(sep);
        List<String> stringList=new ArrayList<String>(strs.length);
        for(String string:strs){
            stringList.add(string);
        }
        return stringList;
    }
}
