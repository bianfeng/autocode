package com.shareyi.jshop.autocode.tableutil;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.shareyi.jshop.autocode.domain.OrderColumn;
import com.shareyi.jshop.autocode.domain.TableModel;

public class TableNameUtil {


	 public  HashMap<String, String> dataTypeMap;
	public  HashMap<String, String> jspTagMap;

	public TableNameUtil(){
		dataTypeMap=new HashMap<String, String>();
		dataTypeMap.put("VARCHAR", "String");
		dataTypeMap.put("VARCHAR2", "String");
		dataTypeMap.put("CHAR", "String");
		dataTypeMap.put("TEXT", "String");
		dataTypeMap.put("BOOLEAN", "Boolean");

		dataTypeMap.put("FLOAT", "Float");
		dataTypeMap.put("DOUBLE", "Double");
		dataTypeMap.put("DECIMAL", "java.math.BigDecimal");

		dataTypeMap.put("TINYINT", "Integer");
		dataTypeMap.put("SMALLINT","Integer");
		dataTypeMap.put("MEDIUMINT","Integer");
		dataTypeMap.put("INT", "Integer");
		dataTypeMap.put("INTEGER", "Integer");
		dataTypeMap.put("BIGINT", "Long");


		dataTypeMap.put("DATE", "java.util.Date");
		dataTypeMap.put("DATETIME", "java.util.Date");
		dataTypeMap.put("TIMESTAMP", "java.sql.Timestamp");

		jspTagMap=new HashMap<String, String>();
		jspTagMap.put("DATE", "DATE");
		jspTagMap.put("DATETIME", "DATETIME");
		jspTagMap.put("TIMESTAMP", "DATETIME");
	}

	 
	/**
	 * 首字符大写，为空返回空
     * 示例： userName->UserName  常用于获取类名
	 * @param str 要转换的字符串
	 * @return 首字符大写
	 */
	public String upperFirst(String str)
	{
		if(str==null){
			return "";
		}
		str=str.trim();
		if(str.length()==0){
			return "";
		}
		
 	return str.substring(0,1).toUpperCase()+str.substring(1);
	}



	
	/**
	 * 首字符小写，为空返回空
     * 示例： UserName  -> userName 常用于类名转实例名
	 * @param str 要转换的字符串
	 * @return 首字符小写
	 */
	public static String lowerCaseFirst(String str)
	{
		if(str==null){
			return "";
		}
		str=str.trim();
		if(str.length()==0){
			return "";
		}
		
 	return str.substring(0,1).toLowerCase()+str.substring(1);
	}
	
	
	/**
	 * 数据库字段名称转换为属性名称 MY_OLD-> myOld
	 * @param dbNames 数据库字段名称
	 * @return 属性名称
	 */
	public String convertToBeanNames(String dbNames)
	{
		 String[] sr=dbNames.split("[_]");
		  StringBuilder builder=new StringBuilder();
		 for(int i=0;i<sr.length;i++)
		 {	
			 String str2=sr[i];
			 if(i!=0)
			 {
				 String c=str2.charAt(0)+"";
			     builder.append(c.toUpperCase()+str2.substring(1).toLowerCase());
			 }
			 else
				 builder.append(str2.toLowerCase());
		   }
		return builder.toString();
	}
	
	
	/**
	 * 转换为数据库字段名称 myOld-> MY_OLD
	 * @param dbNames 属性名称
	 * @return 数据库字段名称
	 */
	public String convertToDbNames(String dbNames)
	{
		 
		  StringBuilder builder=new StringBuilder();
		  List<String> list=splitByUppercase(dbNames);
		 
		  for (String string : list) {
			  builder.append(string.toUpperCase()).append("_");
		}
		   builder.deleteCharAt(builder.length()-1);
		return builder.toString();
	}
	
	
	/**
	 * 将字符串按大写字母拆分为List元素
     * 如： userName -> [user, name]
	 * @param src
	 * @return
	 */
	public List<String> splitByUppercase(String src)
	{
		LinkedList<String> list=new LinkedList<String>();
		if(src==null||src.length()==0){
			return list;
		}
		
		int lastInd=0;
		for(int i=1;i<src.length();i++)
		{
			char c=src.charAt(i);
			if(Character.isUpperCase(c))
			{
				list.add(src.substring(lastInd,i));
				lastInd=i;
			}
		}
		if(lastInd!=src.length())
		{
			list.add(src.substring(lastInd,src.length()));
		}
		return list;
	}




	/**
	 * 根据数据库类型获取java数据类型
     *
     * 如 VARCHAR -> String
	 * @param dbType 数据库数据类型
	 * @return 数据类型
	 */
public String getDataType(String dbType)
  {
	if(dbType==null){
		dbType="";
	}
	
	  String dataType=dataTypeMap.get(dbType.toUpperCase());
	  if(dataType==null)
		  dataType="String";
	  return dataType;
  }



/**
 * 根据数据库类型获取java数据类型 z全包名，  如java.lang.Integer
 * @param dbType 数据库数据类型
 * @return 数据类型
 */
public String getFullDataType(String dbType)
{
	String dataType=this.getDataType(dbType);
	if(dataType.length()>0 && dataType.indexOf(".")<0){
		dataType="java.lang."+dataType;
	}
	return dataType;
}



/**
 * 根据数据库类型获取Tag
 * @param dbType 数据库数据类型
 * @return 数据类型
 */
public String getJspTag(String dbType)
{
if(dbType==null){
	dbType="";
}

  String jspTag=jspTagMap.get(dbType.toUpperCase());
  if(jspTag==null)
	  jspTag="TEXT";
  return jspTag;
}


    /**
     * 获取排序字段
     * @param tableModel
     * @return
     */
	public String getOrderString(TableModel tableModel)
	{
		StringBuilder orderStr=new StringBuilder();
		if(tableModel==null)
			return "";
		
		 List<OrderColumn> orderColumns=tableModel.getOrderColumns();
		 if(orderColumns!=null&&!orderColumns.isEmpty())
		 {
			 orderStr.append(" order by ");
			 for (OrderColumn orderColumn : orderColumns) {
				 orderStr.append(orderColumn.getColumnName());
				 	if("asc".equalsIgnoreCase(orderColumn.getOrderType())) {
				 		 orderStr.append(" asc,");
					 }else{
						 orderStr.append(" desc,");
					 }
						 
			 }
			  orderStr.deleteCharAt(orderStr.length()-1);
		 }
		 
		return orderStr.toString();
	}
	
}
