package com.shareyi.jshop.autocode.tableutil

import com.shareyi.jshop.autocode.domain.Column;
import com.shareyi.jshop.autocode.domain.Dict
import com.shareyi.jshop.autocode.domain.SnippetTemplate;

class SnippetTemplateUtil {

	Map<String,SnippetTemplate>  snippetTemplateMap;
	Map<String,Dict>  dictMap;
	Map<String,Object> binding;
	TemplateUtil templateUtil;

    /**
     * 获取字典项内容
     *
     * 通过column对象中的dictName 查找字典数据
     * 通过字典数据，列定义，模板来生成字典项内容
     * 其中
     * @param column 列定义
     * @param templateId 字典项模板id
     * @return
     */
	public String getDictTemplate(Column column,String templateId){
		Dict dict=dictMap.get(column.dictName);
		if(dict==null){
			return "";
		}
		
		SnippetTemplate snippetTemplate=snippetTemplateMap.get(templateId);
		if(snippetTemplate==null){
			return "";
		}
		
		def templateBinding=["dict":dict,"optionList":dict.optionList,"column":column,"templateId":templateId];
		templateBinding.putAll(binding);
		
		String result="";
		try{
			result= getTemplateUtil().renderContent(snippetTemplate.template, templateBinding);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

    /**
     * 用 列数据+模板片段 生成内容
     * 如果通过模板id未找到模板，则返回空串
     * @param column 列定义
     * @param templateId 模板id
     * @return  生成的内容
     */
	public String getTemplate(Column column,String templateId){
		SnippetTemplate snippetTemplate=snippetTemplateMap.get(templateId);
		if(snippetTemplate==null){
			return "";
		}
		
		def templateBinding=["column":column,"templateId":templateId];
		templateBinding.putAll(binding);
		String result="";
		try{
			result= getTemplateUtil().renderContent(snippetTemplate.template, templateBinding);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}



	/**
	 * 用 模板片段 生成内容
	 * 如果通过模板id未找到模板，则返回空串
	 * @param templateId 模板id
	 * @return  生成的内容
	 */
	public String getTemplate(String templateId){
		SnippetTemplate snippetTemplate=snippetTemplateMap.get(templateId);
		if(snippetTemplate==null){
			return "";
		}

		def templateBinding=["templateId":templateId];
		templateBinding.putAll(binding);
		String result="";
		try{
			result= getTemplateUtil().renderContent(snippetTemplate.template, templateBinding);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}




	/**
	 * 用 模板片段 生成内容
	 * 如果通过模板id未找到模板，则返回空串
	 * @param dataMap
	 * @param templateId 模板id
	 * @return  生成的内容
	 */
	public String getTemplateByMap(Map dataMap,String templateId){
		SnippetTemplate snippetTemplate=snippetTemplateMap.get(templateId);
		if(snippetTemplate==null){
			return "";
		}

		if(dataMap==null){
			dataMap=[:];
		}
		dataMap.put("templateId",templateId);
		dataMap.putAll(binding);
		String result="";
		try{
			result= getTemplateUtil().renderContent(snippetTemplate.template, dataMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}



	/**
	 * 用 模板片段 生成内容
	 * 如果通过模板id未找到模板，则返回空串
	 * @param dataMap
	 * @param templateId 模板id
	 * @return  生成的内容
	 */
	public String getTemplateByData(Object data,String templateId){
		SnippetTemplate snippetTemplate=snippetTemplateMap.get(templateId);
		if(snippetTemplate==null){
			return "";
		}

		def templateBinding=["templateId":templateId,"data":data];
		templateBinding.putAll(binding);
		String result="";
		try{
			result= getTemplateUtil().renderContent(snippetTemplate.template, templateBinding);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}



	public TemplateUtil getTemplateUtil(){
		if(templateUtil==null){
			templateUtil=new TemplateUtil();
		}
		return templateUtil;
	}
}
