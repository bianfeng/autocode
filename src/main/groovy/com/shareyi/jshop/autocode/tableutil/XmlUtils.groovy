package com.shareyi.jshop.autocode.tableutil

import com.shareyi.autocode.enumtype.EngineType
import com.shareyi.jshop.autocode.domain.RequireConfig
import org.apache.commons.lang.StringUtils;

import java.util.LinkedList;
import java.util.Map;

import com.shareyi.jshop.autocode.domain.AutoMake;
import com.shareyi.joywindow.sys.utils.AutoCodeFileUtil;
import com.shareyi.jshop.autocode.domain.Column;
import com.shareyi.jshop.autocode.domain.Dict;
import com.shareyi.jshop.autocode.domain.Option;
import com.shareyi.jshop.autocode.domain.OrderColumn;
import com.shareyi.jshop.autocode.domain.SnippetTemplate
import com.shareyi.jshop.autocode.domain.TableDefine;
import com.shareyi.jshop.autocode.domain.TableModel;
import com.shareyi.jshop.autocode.domain.Template;
import com.shareyi.jshop.autocode.domain.AutoMake;
import com.shareyi.fileutil.FileUtil;

class XmlUtils {
	
	
	/**
	 * 解析tableModel xml文件，将其数据封装为TableModel模型中
	 * @param filePath tableModel xml文件路径
	 * @return TableModel数据封装
	 */
	public static TableModel getTableModelByFile(String filePath)
	{
		File tableModelFile=new File(filePath);
		def tableModelXml=new XmlParser().parse(tableModelFile);
		TableModel tableModel=new TableModel();
		
		def tableDefine=tableModelXml.tableDefine;
		if(tableDefine!=null&&tableDefine!=[])
		{
			tableDefine=tableDefine[0];
			TableDefine tableDefine2=new TableDefine();
			LinkedList<Column> columns=new LinkedList<Column>()
			tableDefine2.setColumns(columns);
			tableDefine2.cnname=tableDefine.attribute("cnname");
			tableDefine2.pageSize=tableDefine.attribute("pageSize");
			tableDefine2.id=tableDefine.attribute("id");
			tableDefine2.dbTableName=tableDefine.attribute("dbTableName");
			tableDefine2.isPaged=tableDefine.attribute("isPaged");
			tableModel.tableDefine=tableDefine2;
			
			tableDefine.column.each{
				Column column=new Column();
				column.dataName=it.attribute("dataName");
				column.columnName=it.attribute("columnName");
				column.jspTag=it.attribute("jspTag");
				column.cnname=it.attribute("cnname");
				column.canBeNull=it.attribute("canBeNull")=='true';
				column.readonly=it.attribute("readonly");
				column.isPK=it.attribute("isPK")=='true';
				column.setLength(it.attribute("length"));
				column.columnType=it.attribute("columnType");
				column.dictName=it.attribute("dictName");
				column.comment=it.attribute("comment");
				columns.add(column);
			}
		}
		
		tableModel.searchKeys=tableModelXml.searchKeys[0]?.text();
		tableModel.allColumn=tableModelXml.allColumn[0]?.text();
		tableModel.queryList=tableModelXml.queryList[0]?.text();
		tableModel.addList=tableModelXml.addList[0]?.text();
		tableModel.updateList=tableModelXml.updateList[0]?.text();
		tableModel.viewList=tableModelXml.viewList[0]?.text();
		tableModel.createTime=tableModelXml.createTime[0]?.text();
		tableModel.updateTime=tableModelXml.updateTime[0]?.text();
		
		LinkedList<OrderColumn> orderColumns=new LinkedList<OrderColumn>()
		tableModel.orderColumns=orderColumns;
		def orderColumns2=tableModelXml.orderColumns;
		if(orderColumns2!=null&&orderColumns2!=[])
		{
			orderColumns2.orderColumn.each{
				OrderColumn orderColumn=new OrderColumn();
				orderColumn.orderType=it.attribute("orderType");
				orderColumn.columnName=it.text();
				orderColumns.add(orderColumn);
			}
		}

		Map<String, Dict> dictMap=[:];
		def dictElements=tableModelXml.dicts.dict;
		if(dictElements!=null && dictElements.size()>0){
			dictElements.each{
				Dict dict=new Dict();
				dict.name=it.attribute("name");
				dict.id=it.attribute("id");
				dictMap.put(dict.id, dict);
				
				List options=dict.optionList;
				it.option.each{
						Option option=new Option();
						option.value=it.attribute("value");
						option.cssClass=it.attribute("cssClass");
						option.name=it.text();
						options.add option;
						print option.name+" "+option.value;
					}
			}
		}
		tableModel.dictMap=dictMap;
		
		return tableModel;
	}
	
	
	/**
	 * 解析autoMake配置文件，封装为AutoMake实例
	 * @param filePath autoMake配置文件路径
	 * @return AutoMake实例
	 */
	public static AutoMake getAutoMake(String filePath,String templateBaseDir)
	{
		if(filePath==null){
			return null;
		}
		File autoXmlFile=new File(filePath);
		def autoMakeXml=new XmlParser().parse(autoXmlFile);
		AutoMake autoMake=new AutoMake();
		
		
		def propsNodes=autoMakeXml.properties;
		if(propsNodes!=null&&propsNodes[0]!=null){
			propsNodes.each {
				def props=it.value();
				props.each { prop->
					autoMake.addProp(prop.name(), prop.text());
				}
			}
		}
		
		List<Template> templates=[];
		autoMake.templates=templates;
		if(autoMakeXml.baseDir!=null && autoMakeXml.baseDir[0]!=null){
			autoMake.baseDir=autoMakeXml.baseDir[0].text();
		}
		
		
		
		
		autoMakeXml.template.each{
			Template t=new Template();
			t.id=it.attribute("id");
			t.desc=it.attribute("desc");
			t.destFile=it.attribute("destFile");
			String engine=it.attribute("engine");
			EngineType engineType=EngineType.getEngineType(engine);
			if(engineType==null){
				engineType=EngineType.GROOVY;
			}
			t.engine=engineType.getType();
			t.templateFile=it.text();
			t.templateFile=FileUtil.contactPath(templateBaseDir,t.templateFile?.trim());
			templates.add t;
		}
		
		return autoMake;
	}
	
	
	/**
	 * 解析autoMake配置文件，封装为AutoMake实例
	 * @param filePath autoMake配置文件路径
	 * @return AutoMake实例
	 */
	public static Map getDictMap(String filePath){
		if(filePath==null){
			return [:];
		}
		File dictFile=new File(AutoCodeFileUtil.parseFilePath(filePath));
		if(!dictFile.exists()){
			return [:];
		}
		def dictXml=new XmlParser().parse(dictFile);
		Map<String, Dict> dictMap=[:];	
		dictXml.dict.each{
			Dict dict=new Dict();
			dict.name=it.attribute("name");
			dict.id=it.attribute("id");
			dictMap.put(dict.id, dict);
			
			List options=dict.optionList;
			it.option.each{
					Option option=new Option();
					option.value=it.attribute("value");
					option.cssClass=it.attribute("cssClass");
					option.name=it.text();
					options.add option;
					print option.name+" "+option.value;
				}
		}
		
		return dictMap;
	}
	
	
	/**
	 * 获取代码片段数据(文本内容获取也在复用，目前只是用了 ID和内容两个值)
	 * @param filePath
	 * @return
	 */
	public static Map getSnippetTemplateMap(String filePath){
		if(filePath==null){
			return [:];
		}
		File snippetTemplateFile=new File(AutoCodeFileUtil.parseFilePath(filePath));
		if(!snippetTemplateFile.exists()){
			return [:];
		}
		def snippetTemplateXml=new XmlParser().parse(snippetTemplateFile);
		Map<String, Dict> snippetTemplateMap=[:];
		snippetTemplateXml.snippetTemplate.each{
			SnippetTemplate snippetTemplate=new SnippetTemplate();
			snippetTemplate.name=it.attribute("name");
			snippetTemplate.id=it.attribute("id");
			snippetTemplate.group=it.attribute("group");
			snippetTemplate.template=it.text();
			snippetTemplateMap.put(snippetTemplate.id, snippetTemplate);
		}
		
		return snippetTemplateMap;
		
	}



	/**
	 * 获取代码片段数据(文本内容获取也在复用，目前只是用了 ID和内容两个值)
	 * @param filePath
	 * @return
	 */
	public static Map readKeyMapFile(String filePath){
		if(filePath==null){
			return [:];
		}
		File keyValueFile=new File(AutoCodeFileUtil.parseFilePath(filePath));
		if(!keyValueFile.exists()){
			return [:];
		}
		def keyValueXml=new XmlParser().parse(keyValueFile);
		Map<String, Dict> keyValueMap=[:];
		keyValueXml.keyValue.each{
			String key=it.attribute("key");
			String value=it.text();
			keyValueMap.put(key, value);
		}

		return keyValueMap;

	}




	/**
	 * 获取代码片段数据(文本内容获取也在复用，目前只是用了 ID和内容两个值)
	 * @param filePath
	 * @return
	 */
	public static Map readRequireConfigFile(String filePath){
		if(filePath==null){
			return [:];
		}
		File keyValueFile=new File(AutoCodeFileUtil.parseFilePath(filePath));
		if(!keyValueFile.exists()){
			return [:];
		}
		def keyValueXml=new XmlParser().parse(keyValueFile);
		Map<String, RequireConfig> keyValueMap=[:];
		keyValueXml.requireConfig.each{

			RequireConfig requireConfig=new RequireConfig();
			requireConfig.columnTag=it.attribute("columnTag");
			requireConfig.desc=it.attribute("desc");
			requireConfig.requireModule=it.attribute("requireModule");
			String sortNum=it.attribute("sortNum");
			if(StringUtils.isNumeric(sortNum)){
				requireConfig.sortNum=Integer.parseInt(sortNum);
			}


			requireConfig.requireText=it.text();
			keyValueMap.put(requireConfig.columnTag, requireConfig);
		}

		return keyValueMap;

	}
}
