package com.shareyi.jshop.autocode.service;


public interface FileNameFilter {
		
	/**
	 * 列是否符合过滤器
	 * @param name
	 * @return
	 */
	public boolean isMatch(String name);
}
