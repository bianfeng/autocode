package com.shareyi.autocode.action;

import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.shareyi.joywindow.sys.action.BaseAction;
import com.shareyi.joywindow.sys.utils.AutoCodeFileUtil;
import com.shareyi.joywindow.sys.utils.BindResource;
import com.shareyi.jshop.autocode.service.CopyAndReplaceService;
import com.shareyi.jshop.autocode.utils.AutoCodeConstant;

/**
 *  文件内容及目录替换功能
 */
public class ReplaceAction extends BaseAction {

	private String replaceExp,replaceType;

	/**
	 * 文件路径替换表达式
	 */
	private String dirReplaceExp;



	public String replacePage(){
		Properties  replacePro=BindResource.getBindProperties(AutoCodeConstant.BIND_REPLACEUTIL);
		Properties  replaceInfoPro=BindResource.getBindProperties(AutoCodeConstant.BIND_REPLACEINFO);
		Map resultMap=result.getResult();

		if(replacePro!=null){
			resultMap.putAll(replacePro);
		}
		if(replaceInfoPro!=null){
			resultMap.putAll(replaceInfoPro);
		}

		return view(SUCCESS, result);
	}
	
	
	
	public void doReplace(){
		Properties  replacePro=BindResource.getBindProperties(AutoCodeConstant.BIND_REPLACEUTIL);
		String srcPath=replacePro.getProperty("pro_src_path");
		String destPath=replacePro.getProperty("pro_dest_path");

		String ignoreExp=replacePro.getProperty("pro_ignore_exp");
		String throwExp=replacePro.getProperty("pro_throw_exp");
		srcPath=AutoCodeFileUtil.parseFilePath(srcPath);
		destPath=AutoCodeFileUtil.parseFilePath(destPath);
		if(StringUtils.isEmpty(srcPath) || StringUtils.isEmpty(destPath)){
			result.setMessage("参数有误！");
		}else{
			try {
				result=CopyAndReplaceService.doCopyAndReplace(srcPath, destPath, ignoreExp, throwExp, replaceType, replaceExp, dirReplaceExp);
			} catch (Exception e) {
				e.printStackTrace();
				result.setMessage(e.getMessage());
			}
		}
		
		sendResultJson(result);
	}
	
	
	public String getReplaceExp() {
		return replaceExp;
	}



	public void setReplaceExp(String replaceExp) {
		this.replaceExp = replaceExp;
	}



	public String getReplaceType() {
		return replaceType;
	}



	public void setReplaceType(String replaceType) {
		this.replaceType = replaceType;
	}


	public String getDirReplaceExp() {
		return dirReplaceExp;
	}

	public void setDirReplaceExp(String dirReplaceExp) {
		this.dirReplaceExp = dirReplaceExp;
	}
}
