package com.shareyi.jshop.word.dataprocess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LineDataProcess
  implements DataProcess
{
	public Map<String, Object> processData(String dataSrc, boolean qutoflag) {
		HashMap<String,Object> map = new HashMap<String,Object>();
		List<String> dataList = new ArrayList<String>();

		String[] cols = dataSrc.split("\n");
		for (String col : cols) {
			col = col.trim();
			if (col.startsWith("##*"))
				continue;
			if(col.length()>0){
				dataList.add(col);
			}
			
		}
		map.put("dataList", dataList);
		return map;
	}

}