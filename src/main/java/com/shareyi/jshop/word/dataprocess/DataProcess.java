package com.shareyi.jshop.word.dataprocess;


import java.util.Map;

public abstract interface DataProcess
{
  public abstract Map<String, Object> processData(String paramString,boolean qutoFlag);
}