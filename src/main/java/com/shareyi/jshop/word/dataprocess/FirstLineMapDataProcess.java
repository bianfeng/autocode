package com.shareyi.jshop.word.dataprocess;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class FirstLineMapDataProcess extends SimpleDataProcess
  implements DataProcess
{
  public Map<String, Object> processData(String dataSrc,boolean qutoFlag)
  {
    Map dataMap = super.processData(dataSrc,qutoFlag);

    List list = (List)dataMap.get("dataList");

    if (list.size() > 0)
    {
      List firstList = (List)list.get(0);
      list.remove(0);
      HashMap configMap = ProccessListToMap(firstList);
      dataMap.put("configMap", configMap);
    }

    return dataMap;
  }

  private HashMap<String, String> ProccessListToMap(List<String> firstList)
  {
    HashMap configMap = new HashMap();

    for (Iterator localIterator = firstList.iterator(); localIterator.hasNext(); ) {
     String keyValue = (String)localIterator.next();
      String[] strArray = keyValue.split("=");
      if (strArray.length > 1) {
        configMap.put(strArray[0], strArray[1]);
      }else{
    	  configMap.put(strArray[0], "");
      }
    }

    return configMap;
  }
}