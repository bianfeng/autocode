package com.shareyi.jshop.word.dataprocess;

import java.util.List;
import java.util.Map;

public class FirstLineDiffDataProcess extends SimpleDataProcess
  implements DataProcess
{
  public Map<String, Object> processData(String dataSrc,boolean qutoFlag)
  {
    Map map = super.processData(dataSrc,qutoFlag);

    List list = (List)map.get("dataList");

    if (list.size() > 0)
    {
      List firstList = (List)list.get(0);
      list.remove(0);
      map.put("firtList", firstList);
    }

    return map;
  }
}