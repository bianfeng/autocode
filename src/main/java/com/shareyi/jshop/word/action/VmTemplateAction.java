package com.shareyi.jshop.word.action;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.shareyi.jshop.autocode.tableutil.TemplateUtil;
import com.shareyi.jshop.word.dataprocess.DataProcess;
import com.shareyi.jshop.word.dataprocess.FirstLineDiffDataProcess;
import com.shareyi.jshop.word.dataprocess.FirstLineMapDataProcess;
import com.shareyi.jshop.word.dataprocess.LineDataProcess;
import com.shareyi.jshop.word.dataprocess.SimpleDataProcess;
import com.shareyi.wox.context.ValueContext;
import com.shareyi.joywindow.sys.action.BaseAction;
import com.shareyi.simpleserver.core.HttpRequest;
import com.shareyi.simpleserver.core.HttpResponse;

public class VmTemplateAction extends BaseAction{
	
static HashMap<String, DataProcess> dataProcessMap = new HashMap<String, DataProcess>();

//模板引擎类型
private String engineType;

String dataProcessId;


private String dataSrc;
private String vmRes;
private String templateContent;
private boolean qutoFlag;


static
{
  dataProcessMap.put("simpleDataProcess", new SimpleDataProcess());
  dataProcessMap.put("firstLineDataProcess", new FirstLineDiffDataProcess());
  dataProcessMap.put("firstLineMapDataProcess", new FirstLineMapDataProcess());
  dataProcessMap.put("lineDataProcess", new LineDataProcess());
}


/**
 * 渲染模板内容
 * @throws Exception
 */
public void mergeTemplate() throws Exception{
	
  if(StringUtils.isEmpty(templateContent)){
	  	result.setSuccess(false);
	  	result.setMessage("模板内容为空，无法执行！");
	  	sendResultJson(result);
	  	return;
	}
  
  
  ValueContext valueContext=this.valueContext();
  
  if(StringUtils.isNotEmpty(dataSrc)){
	   DataProcess dataProcess = (DataProcess)dataProcessMap.get(this.dataProcessId);
	   if (dataProcess == null){
	     dataProcess = new SimpleDataProcess();
	     dataProcessMap.put(this.dataProcessId, dataProcess);
	   }

	   Map dataMap = dataProcess.processData(this.dataSrc,qutoFlag);
	   Set keySet = dataMap.keySet();
	   for (Iterator localIterator = keySet.iterator(); localIterator.hasNext(); ) { 
	 	  String key = (String)localIterator.next();
	     	valueContext.setValue(key, dataMap.get(key));
	   }
	}
    
  	try{
  		String content="";
  		if("groovy".equals(engineType)){
  			content=mergeTemplateGroovy(templateContent,valueContext);
  		}else{
  			content=mergeTemplate(templateContent,valueContext);
  		}
  		
  		result.addModel("content", content);
  		result.setSuccess(true);
  	}catch(Exception e){
  		result.setSuccess(false);
  		result.setMessage("执行失败，模板可能有问题，原因是："+e.getMessage());
  	}
  	sendResultJson(result);
  	
}

	private String mergeTemplate(String templateContent2, ValueContext valueContext) {
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(Velocity.INPUT_ENCODING,"utf-8");
		ve.setProperty(Velocity.OUTPUT_ENCODING,"utf-8");
		ve.init();
		StringWriter writer=new StringWriter();
		ve.evaluate(createContext(valueContext, getRequest(), getResponse()), writer, VmTemplateAction.class.getName(), templateContent2);
		return writer.toString();
		
  }

	private String mergeTemplateGroovy(String templateContent2, ValueContext valueContext) {
		TemplateUtil templateUtil=new TemplateUtil();
		Map<String,Object> context=valueContext.getContext();
		request=getRequest();
		context.put("request",request);
		context.put("requestAttr", request.getAttributeMap());
		String result=templateUtil.renderContent(templateContent2, context);
		return result;
		
  }
	protected Context createContext(ValueContext valueContext,
			HttpRequest request, HttpResponse response) {
		   	VelocityContext context=new VelocityContext(valueContext.getContext());
			context.put("request",request);
			context.put("requestAttr", request.getAttributeMap());
			return context;
	}
	
	public String getDataSrc()
	{
	  return this.dataSrc; }
	
	public void setDataSrc(String dataSrc) {
	  this.dataSrc = dataSrc;
	}
	
	public String getDataProcessId()
	{
	  return this.dataProcessId;
	}
	
	public void setDataProcessId(String dataProcessId)
	{
	  this.dataProcessId = dataProcessId;
	}
	
	public String getVmRes()
	{
	  return this.vmRes;
	}
	
	public void setVmRes(String vmRes)
	{
	  this.vmRes = vmRes;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public boolean isQutoFlag() {
		return qutoFlag;
	}

	public void setQutoFlag(boolean qutoFlag) {
		this.qutoFlag = qutoFlag;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}
}