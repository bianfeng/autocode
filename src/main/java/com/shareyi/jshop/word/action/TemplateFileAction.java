package com.shareyi.jshop.word.action;

import java.io.File;
import com.shareyi.joywindow.JoyWindowData;
import com.shareyi.joywindow.sys.action.BaseAction;

public class TemplateFileAction extends BaseAction {

	private static final String TEMPATES_DIR = "/vm/word/template";
	private String fileName;
	private String fileContent;



	/**
	 * 获取文件列表页
	 * @return
	 */
	public String templateListPage(){
			File file=new File(JoyWindowData.getHttpServer().getBasePath(),TEMPATES_DIR);
			if(file.exists()&&file.isDirectory()){
				String path=file.getAbsolutePath();
				result.addModel("templatePath", path);
			}
			return view(SUCCESS, result);
		}
	
	
	

	
	
	public String getFileName() {
		return fileName;
	}




	public void setFileName(String fileName) {
		this.fileName = fileName;
	}




	public String getFileContent() {
		return fileContent;
	}




	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}



}
