package com.shareyi.jshop.test.action;

import javax.annotation.Resource;

import com.shareyi.joywindow.sys.action.BaseAction;

public class SpringAction extends BaseAction{

	@Resource
	private TestService testService;
	
	
	public void helloWorld(){
		result.setSuccess(true);
		result.setMessage(testService.helloworld());
		sendResultJson(result);
	}
}
