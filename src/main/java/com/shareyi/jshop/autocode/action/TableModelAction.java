package com.shareyi.jshop.autocode.action;

import java.util.List;
import java.util.Properties;

import com.shareyi.jshop.autocode.domain.SimpleTableInfo;
import org.apache.commons.lang.StringUtils;

import com.shareyi.jshop.autocode.dbutil.DBTableUtil;
import com.shareyi.jshop.autocode.utils.AutoCodeConstant;
import com.shareyi.joywindow.sys.action.BaseAction;
import com.shareyi.joywindow.sys.utils.BindResource;

public class TableModelAction extends BaseAction {

	
	private String tableModelDir;
	private String tableName;
	private String cnname;

	private int smartFlag=1;



	/**
	 * 生成tableModel
	 */
	public void getTableList(){
		Properties  databasePro=BindResource.getBindProperties(AutoCodeConstant.BIND_DATABASE);
		String driverName=databasePro.getProperty("pro_jdbc_driver");
		String url=databasePro.getProperty("pro_jdbc_url");
		String username=databasePro.getProperty("pro_jdbc_username");
		String password=databasePro.getProperty("pro_jdbc_password");
		if(StringUtils.isEmpty(driverName)||StringUtils.isEmpty(url)
				||StringUtils.isEmpty(username)||StringUtils.isEmpty(password)){
			result.setMessage("参数有误,请检查数据库配置！");
		}else{
			try {
				List<SimpleTableInfo> list=DBTableUtil.getTableList(driverName, url, username, password);
				result.setSuccess(true);
				result.addModel("tableList",list);
			} catch (Exception e) {
				e.printStackTrace();
				result.setMessage(e.getMessage());
			}
		}
		sendResultJson(result);
	}


	/**
	 * 生成tableModel
	 */
	public void makeTableModel(){
		
		Properties  databasePro=BindResource.getBindProperties(AutoCodeConstant.BIND_DATABASE);
		String driverName=databasePro.getProperty("pro_jdbc_driver");
		String url=databasePro.getProperty("pro_jdbc_url");
		String username=databasePro.getProperty("pro_jdbc_username");
		String password=databasePro.getProperty("pro_jdbc_password");
		if(StringUtils.isEmpty(tableModelDir)||StringUtils.isEmpty(driverName)||StringUtils.isEmpty(url)
				||StringUtils.isEmpty(username)||StringUtils.isEmpty(password)||StringUtils.isEmpty(tableName)){
			result.setMessage("参数有误！");
		}else{
			try {
				String message=DBTableUtil.generateDbTableModel(tableName, cnname,driverName, url, username, password, tableModelDir,smartFlag);
				result.setSuccess(true);
				result.setMessage(message);
			} catch (Exception e) {
				e.printStackTrace();
				result.setMessage(e.getMessage());
			}
		}
		
		sendResultJson(result);
	}

	public String getTableModelDir() {
		return tableModelDir;
	}

	public void setTableModelDir(String tableModelDir) {
		this.tableModelDir = tableModelDir;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getSmartFlag() {
		return smartFlag;
	}

	public void setSmartFlag(int smartFlag) {
		this.smartFlag = smartFlag;
	}


	public String getCnname() {
		return cnname;
	}

	public void setCnname(String cnname) {
		this.cnname = cnname;
	}
}
