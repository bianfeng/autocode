package com.shareyi.jshop.autocode.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.shareyi.jshop.autocode.domain.AutoCodeParams;
import org.apache.commons.lang.StringUtils;
import com.shareyi.jshop.autocode.tableutil.AutoCodeUtil;
import com.shareyi.jshop.autocode.domain.Config;
import com.shareyi.jshop.autocode.utils.AutoCodeConstant;
import com.shareyi.joywindow.sys.action.BaseAction;
import com.shareyi.joywindow.sys.utils.BindResource;

/**
 * 执行代码生成的Action
 */
public class AutoMakeAction extends BaseAction {

	
	private String tableModelPath;
	private String baseDir;
	private String overideFile;
	private String[] templateIds;

	/**
	 * 根据模板生成页面
	 */
	public void doAutoMake(){
		
		Properties  autoXmlPro=BindResource.getBindProperties(AutoCodeConstant.BIND_AUTOXML);
		
		String autoxmlPath=autoXmlPro.getProperty("pro_autoxml_path");
		String templateBaseDir=autoXmlPro.getProperty("pro_template_basedir");
		if(StringUtils.isEmpty(autoxmlPath)||StringUtils.isEmpty(templateBaseDir)||StringUtils.isEmpty(tableModelPath)
				||StringUtils.isEmpty(baseDir)){
			result.setMessage("autoMake.xml或者模板根目录为空");
		}else{
			try {

				//封装自动代码相关参数
                AutoCodeParams autoMakeParams=new AutoCodeParams();
				autoMakeParams.autoXmlPath=autoxmlPath;
                autoMakeParams.tableModelPath=tableModelPath;
                autoMakeParams.baseDir=baseDir;
                autoMakeParams.templateBaseDir=templateBaseDir;
                autoMakeParams.config=this.getConfig();
                autoMakeParams.overideFlag="true".equals(overideFile);
                autoMakeParams.templateIds=templateIds;

                String generatedFile=AutoCodeUtil.generateCodeByTableModelFile(autoMakeParams);
				result.setSuccess(true);
				result.setMessage(generatedFile);
			} catch (Exception e) {
				result.setMessage(e.getMessage());
			}
		}
		
		sendResultJson(result);
	}

	
	private Config getConfig(){
		Config config=new Config();
		Properties autoMakePro=BindResource.getBindProperties(AutoCodeConstant.BIND_AUTOMAKE);
		String author=autoMakePro.getProperty("pro_code_username");
		String date=autoMakePro.getProperty("pro_code_date");
		String year=autoMakePro.getProperty("pro_code_year");
		String style=autoMakePro.getProperty("pro_code_style");
		String category=autoMakePro.getProperty("pro_code_category");
		String basePackage=autoMakePro.getProperty("pro_code_basePackage");
		String artifactId=autoMakePro.getProperty("pro_code_artifactId");

		config.setAuthor(author);
		String systemDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		if(StringUtils.isBlank(date)){
			date=systemDate;
		}
		if(StringUtils.isBlank(year)){
			year=systemDate.substring(0,4);
		}
		if(StringUtils.isEmpty(category)){
			category="";
		}
		config.setAuthor(author);
		config.setNowDate(date);
		config.setYear(year);
		config.setStyle(style);
		config.setCategory(category);
		config.setBasePackage(basePackage);
		config.setArtifactId(artifactId);
		
		return config;
	}
	
	public String getTableModelPath() {
		return tableModelPath;
	}

	public void setTableModelPath(String tableModelPath) {
		this.tableModelPath = tableModelPath;
	}

	public String getBaseDir() {
		return baseDir;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}

	public String getOverideFile() {
		return overideFile;
	}

	public void setOverideFile(String overideFile) {
		this.overideFile = overideFile;
	}


    public String[] getTemplateIds() {
        return templateIds;
    }

    public void setTemplateIds(String[] templateIds) {
        this.templateIds = templateIds;
    }
}
