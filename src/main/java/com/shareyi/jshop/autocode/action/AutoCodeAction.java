package com.shareyi.jshop.autocode.action;

import java.util.Map;
import java.util.Properties;

import com.shareyi.jshop.autocode.domain.AutoMake;
import com.shareyi.jshop.autocode.tableutil.AutoCodeUtil;
import com.shareyi.jshop.autocode.utils.AutoCodeConstant;
import com.shareyi.fileutil.FileUtil;
import com.shareyi.joywindow.sys.action.BaseAction;
import com.shareyi.joywindow.sys.utils.BindResource;
import org.apache.commons.lang.StringUtils;


/**
 * 执行代码生成配置页面的action
 */
public class AutoCodeAction extends BaseAction {


    /**
     * 自动代码生成参数设置页面
     * @return
     */
	public String config(){
		Properties  databasePro=BindResource.getBindProperties(AutoCodeConstant.BIND_DATABASE);
		Properties  dirPro=BindResource.getBindProperties(AutoCodeConstant.BIND_DIR);
		Properties  autoXmlPro=BindResource.getBindProperties(AutoCodeConstant.BIND_AUTOXML);
		Map resultMap=result.getResult();
		if(databasePro!=null){
			resultMap.putAll(databasePro);
		}
		if(dirPro!=null){
			resultMap.putAll(dirPro);
		}
		if(autoXmlPro!=null){
			resultMap.putAll(autoXmlPro);
		}else{
			result.addModel("pro_autoxml_path",FileUtil.getRuntimeFilePath("/config/autocode/autoMake.xml"));
		}
		return view(SUCCESS, result);
	}


    /**
     * 生成tableModel页面
     * @return
     */
	public String tableModel(){
		Properties  databasePro=BindResource.getBindProperties(AutoCodeConstant.BIND_DATABASE);
		Properties  dirPro=BindResource.getBindProperties(AutoCodeConstant.BIND_DIR);
		Map resultMap=result.getResult();

		if(databasePro!=null){
			resultMap.putAll(databasePro);
		}
		if(dirPro!=null){
			resultMap.putAll(dirPro);
		}
		return view(SUCCESS, result);
	}


    /**
     * 自动生成代码页面
     * @return
     */
	public String autoMake(){
		Properties  dirPro=BindResource.getBindProperties(AutoCodeConstant.BIND_DIR);
		Properties  autoXmlPro=BindResource.getBindProperties(AutoCodeConstant.BIND_AUTOXML);
		Properties  autoMakePro=BindResource.getBindProperties(AutoCodeConstant.BIND_AUTOMAKE);
		Map resultMap=result.getResult();
		if(dirPro!=null){
			resultMap.putAll(dirPro);
		}
		if(autoMakePro!=null){
			resultMap.putAll(autoMakePro);
		}

		if(autoXmlPro!=null){
			resultMap.putAll(autoXmlPro);

            //获取autoMake对象
            String autoXmlPath=autoXmlPro.getProperty("pro_autoxml_path");
            String templateBasedir=autoXmlPro.getProperty("pro_template_basedir");
            if(StringUtils.isNotEmpty(autoXmlPath)||StringUtils.isNotEmpty(templateBasedir)){
                AutoMake autoMake= AutoCodeUtil.getAutoMake(autoXmlPath, templateBasedir);
                result.addModel("autoMake",autoMake);
            }

		}



      //  result.addModel("pro_autoxml_path",FileUtil.getRuntimeFilePath("/config/autocode/autoMake.xml"));

        result.setSuccess(true);
        return view(SUCCESS, result);
	}


}
