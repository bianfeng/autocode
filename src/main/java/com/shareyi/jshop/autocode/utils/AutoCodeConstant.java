package com.shareyi.jshop.autocode.utils;

public class AutoCodeConstant {

	
	public static final String BIND_DATABASE="autocode_database";
	public static final String BIND_DIR="autocode_dir";
	public static final String BIND_AUTOXML="autocode_autoXml";
	public static final String BIND_AUTOMAKE = "autocode_autoMake";
	public static final String BIND_DBTABLEUTIL="autocode_dbTableUtil";

	/**
	 * 替换工具的bindId
	 */
	public static final String BIND_REPLACEUTIL="autocode_replaceUtil";
	/**
	 * 替换参数的bindId
	*/
	public static final String BIND_REPLACEINFO = "autocode_replaceInfo";

	public static final String DICT_PATH = "dict.path";
	/** 代码片段配置项*/
	public static final String SNIPPET_TEMPLATE_PATH = "snippet.template.path";


	/** 静态text文本配置资源*/
	public static final String EXTEND_TEXT_CONFIG_PATH = "extend.text.config";



	/** 静态text文本配置资源*/
	public static final String REQUIRE_JS_MAP_PATH = "pagerequire.map.config";


	/**数据库类型到Java类型映射*/
	public static final String DBTYPE_JAVA_MAP_PATH = "dbtype.javatype.map.config";



	/** 数据库类型到页面类型映射*/
	public static final String DBTYPE_COLUMNTAG_MAP_PATH = "dbtype.columntag.map.config";

}
