package com.shareyi.joywindow.sys.action;

import java.util.HashMap;
import java.util.Map;

import com.shareyi.wox.action.WoxActionSupport;
import com.shareyi.wox.context.ServletActionContext;
import com.shareyi.wox.context.ValueContext;
import com.shareyi.joywindow.sys.domain.Result;
import com.shareyi.joywindow.sys.utils.JsonUtils;
import com.shareyi.simpleserver.core.HttpRequest;
import com.shareyi.simpleserver.core.HttpResponse;


/**

 * 基础Action
 * @author ZhaoHongbo
 * @date 2012-12-21 下午02:04:31
 *
 */
public class BaseAction extends WoxActionSupport {

	private static final long serialVersionUID = 686019132160790345L;
	public static final String RESULT_KEY = "result";
	public static final String MESSAGE_KEY = "message";
	
	protected HttpResponse response;
	protected HttpRequest request;
	
	protected String path = "/vm";
	
	@SuppressWarnings("rawtypes")
	protected Result result = new Result();
	
	public void setServletRequest(HttpRequest request) {
		this.request = request;
	}

	public void setServletResponse(HttpResponse response) {
		this.response = response;
	}

	/**
	 * 获取真实的IP地址
	 * 
	 */
	public String getRemoteIP() {
		String ip = request.getRemoteAddr();
		if (request.getHeader("x-forwarded-for") != null
				&& !"unknown".equalsIgnoreCase(request.getHeader("x-forwarded-for"))) {
			ip = ip + "," + request.getHeader("x-forwarded-for");
		}
		return ip;
	}
	
	public HttpResponse getResponse() {
		return ServletActionContext.getResponse();
	}

	public HttpRequest getRequest() {
		return ServletActionContext.getRequest();
	} 

	/**
	 * 输出内容到客户端
	 * 
	 * @param obj
	 */
	public void write(Object obj) {
		this.getResponse().getPrintWriter().print(obj);
	}
	
	/**
	 * 输出内容到客户端
	 * 
	 * @param result
	 */
	public void write(@SuppressWarnings("rawtypes") Result result) {
		if (result.isSuccess()) {
			this.write(Boolean.TRUE);
		} else {
			this.write(result.getMessage());
		}
	}
	
	/**
	 * 将内容以JSON形式输出
	 * 
	 * @param obj
	 */
	public void writeToJson(Object obj) {
		this.write(JsonUtils.toJson(obj));
	}
	
	/**
	 * 发送Result对象的json格式结果 result和message
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendResultJson(Result result){
		Map<String, Object> mapContent = new HashMap<String, Object>();
		mapContent.put(RESULT_KEY, result.getSuccess());
		mapContent.put(MESSAGE_KEY, result.getMessage());
		mapContent.putAll(result.getResult());
		writeToJson(mapContent);
	}
	
	/**
	 * 发送操作正确的标识
	 */
	public void sendOK(){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(RESULT_KEY, true);
		this.writeToJson(result);
	}
	
	/**
	 * 发送操作失败的标识
	 */
	public void sendFail(){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(RESULT_KEY, false);
		this.writeToJson(result);
	}
	
	/**
	 * 发送操作失败的json对象标识,包含错误信息
	 */
	public void sendFail(String errorMsg){
		Map<String, Object> mapContent = new HashMap<String, Object>();
		mapContent.put(RESULT_KEY, false);
		mapContent.put(MESSAGE_KEY, errorMsg);
		writeToJson(mapContent);
	}
	
	/**
	 * 设置velocity context并返回到指定action
	 * 
	 * @param result
	 * @param name
	 * @return
	 */
	public String view(String name, @SuppressWarnings("rawtypes") Result result) {
		this.setValueContext(result);
//		if (!result.getSuccess()) {
//			return result.getLocation();
//		}
		return name;
	}
	
	/**
	 * 设置velocity context
	 * 
	 * @param result
	 */
	public void toVm(@SuppressWarnings("rawtypes") Result result) {
		this.setValueContext(result);
	}
	
	/**
	 * 设置Struts2 OGNL值栈
	 * 
	 * @param result
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setValueContext(Result result) {
		ValueContext context = valueContext();
		Map<String, Object> o = result.getResult();
		context.putAll(o);
	}
	
	/**
	 * 返回struts2的值栈
	 * 
	 * @return
	 */
	public ValueContext valueContext() {
		return ServletActionContext.getValueContext();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
}
