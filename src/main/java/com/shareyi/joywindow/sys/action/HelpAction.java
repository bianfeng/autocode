package com.shareyi.joywindow.sys.action;


public class HelpAction extends BaseAction {

	private String helpPath="/vm/sys/help";
	
	public  String execute() throws Exception {
		valueContext().setValue("helpPath",	helpPath);
		return SUCCESS;
	}
	
	
	

	public String getHelpPath() {
		return helpPath;
	}

	public void setHelpPath(String helpPath) {
		this.helpPath = helpPath;
	}
	
}
