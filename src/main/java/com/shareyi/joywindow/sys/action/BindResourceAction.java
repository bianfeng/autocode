package com.shareyi.joywindow.sys.action;


import java.util.Map;
import java.util.Properties;
import java.util.Set;
import com.shareyi.joywindow.sys.utils.BindResource;
import com.shareyi.simpleserver.core.HttpRequest;


public class BindResourceAction extends BaseAction {


	private String bindId;

	public void saveBindResource() throws Exception {
		Properties pro=null;
		try{
		if(bindId!=null)
			{
			 pro=BindResource.getBindProperties(bindId);
			 if(pro==null)
			 {
				 pro=new Properties();
			 }

			 	HttpRequest request=getRequest();
				Set<String> parameterNames=request.getParameterNames();
				for (String key : parameterNames) {
					if(key.startsWith("pro_")){
						pro.setProperty(key,request.getParameter(key));
					}
				}

				boolean b=BindResource.saveBindProperties(bindId, pro);
				result.setSuccess(b);
				if(b)
					result.setMessage("save success!");
				else 
					result.setMessage("save failed!");
			}else
			{
				result.setMessage("save failed!");
			}
		}catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(e.getMessage());
		}

		sendResultJson(result);
		
	}

	public void setBindId(String bindId) {
		this.bindId = bindId;
	}

	public String getBindId() {
		return bindId;
	}

	
	/*
	public static void main(String[] args) {
		Properties pro=new Properties();
		pro.setProperty("good", "test1");
		String str="{\"sdfdf\":\"sdfkldf\",\"good\":\"test\",\"dfls\":\"dfldfs\",\"fsklsdf\":\"tlfdslfd\"}";
		JSONObject jsonObject=JSONObject.fromObject(str);
		HashMap map=(HashMap) JSONObject.toBean(jsonObject, HashMap.class);
		pro.putAll(map);
		System.out.println(pro);
	}
	*/
}
