package com.shareyi.joywindow.sys.action;

import java.awt.*;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;

import com.shareyi.joywindow.sys.utils.OsUtils;
import org.apache.commons.lang.StringUtils;

import com.shareyi.fileutil.FileIo;
import com.shareyi.fileutil.FileUtil;
import com.shareyi.joywindow.JoyWindowData;
import com.shareyi.joywindow.sys.domain.FileInfo;
import com.shareyi.joywindow.sys.utils.AutoCodeFileUtil;
import com.shareyi.joywindow.sys.utils.SystemInvoke;


public class FileUtilAction extends BaseAction {

	private String dialogType; //save,open,directory
	private String fileType; //img,audio,vedio,doc,all,other
	private String fileExt;	 //文件类型
	private String editFilePath;
	private String editFileContent;
	private String parentPath;
	private String changeCurPath;
	
	
	public void getFilePath() {
		
		
		String filePath=null;
		
		//转换文件路径为正确路径
		
		parentPath=AutoCodeFileUtil.parseFilePath(parentPath);
		if(StringUtils.isEmpty(parentPath)){
			parentPath=FileUtil.getRunPath();
		}
		if("open".equals(dialogType))
		{
			File file=new File(parentPath);
			if(file.exists() && file.isFile()){
				parentPath=file.getParent();
			}
			String[] fileExtensions=getExtensions();
			filePath=JoyWindowData.getJoyWindow().getOpenPath(parentPath,true,fileExtensions,fileExtensions);
		}
		else if("directory".equals(dialogType))
		{
			filePath=JoyWindowData.getJoyWindow().getDirectoryPath(parentPath);
		}else //if("save".equals(dialogType))
		{
			String[] fileExtensions=getExtensions();
			filePath=JoyWindowData.getJoyWindow().getSavePath(parentPath,true,fileExtensions,fileExtensions);
		}
		
		if("1".equals(changeCurPath)){
			filePath=AutoCodeFileUtil.changeCurFilePath(filePath);
		}
		result.setSuccess(true);
		result.addModel("filePath", filePath);
		sendResultJson(result);
	}

	private String[] getExtensions() {
		if(fileType==null|"".equals(fileType)||"all".equals(fileType))
			return allExtensions;
		else if("other".equals(fileType))
			{
				if(fileExt==null||"".equals(fileExt))
					return allExtensions;
				else
					return new String[]{"*."+fileExt.trim()};
			}
		else if("img".equals(fileType))
			return imgExtensions;
		else if("audio".equals(fileType))
			return audioExtensions;
		else if("vedio".equals(fileType))
			return vedioExtensions;
		else if("doc".equals(fileType))
			return docExtensions;
		
		return allExtensions;
	}
	
	
	
	public void startFile() {
		if (StringUtils.isEmpty(editFilePath)) {
			result.setSuccess(false);
			result.setMessage("文件路径为空！");
		} else {
			File file = new File(AutoCodeFileUtil.parseFilePath(editFilePath));
			if (!file.exists()) {
				result.setSuccess(false);
				result.setMessage("文件不存在!");
				sendResultJson(result);
				return ;
			}

			if (file.isFile()) {
				try {
					Desktop desktop=Desktop.getDesktop();
					desktop.open(file);
					//Runtime.getRuntime().exec("start " + editFilePath);
				} catch (IOException e) {
				}
				result.setSuccess(true);
				result.setMessage("文件已打开");
			} else {
				result.setMessage("文件为文件夹");
			}
		}

		sendResultJson(result);
	}

	public void editFile() {
		if (StringUtils.isEmpty(editFilePath)) {
			result.setSuccess(false);
			result.setMessage("文件路径为空！");
		} else {
			File file = new File(AutoCodeFileUtil.parseFilePath(editFilePath));
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					result.setSuccess(false);
					result.setMessage("创建文件失败：" + e.getMessage());
					sendResultJson(result);
					return ;
				}
			}

			if (file.isFile()) {
				try {
					Desktop desktop=Desktop.getDesktop();
					desktop.edit(file);
				} catch (IOException e) {
				}
				result.setSuccess(true);
				result.setMessage("文件已打开");
			} else {
				result.setMessage("文件为文件夹");
			}
		}

		sendResultJson(result);
	}
	
	
	
	public void saveFile() {
		if (StringUtils.isEmpty(editFilePath)||"null".equals(editFilePath)) {
			result.setSuccess(false);
			result.setMessage("文件路径为空！");
		} else {
			File file = new File(AutoCodeFileUtil.parseFilePath(editFilePath));
			if (!file.exists()) {
					file=FileUtil.makeSureFileExsit(file);
					if(file==null){
						result.setSuccess(false);
						result.setMessage("创建文件失败："+editFilePath);
						sendResultJson(result);
						return ;	
					}
			}

			if (file.isFile()&&file.canWrite()) {
				FileIo.writeToFile(file, editFileContent, "utf-8");
				result.setSuccess(true);
				result.setMessage("文件保存成功");
			} else {
				result.setMessage("文件无法写入,无法保存！");
			}
		}

		sendResultJson(result);
	}	
	
	
	public void getFileContent() {
		if (StringUtils.isEmpty(editFilePath)) {
			result.setSuccess(false);
			result.setMessage("文件路径为空！");
		} else {
			File file = new File(AutoCodeFileUtil.parseFilePath(editFilePath));
			if (!file.exists()) {
						result.setSuccess(false);
						result.setMessage("文件不存在："+editFilePath);
						sendResultJson(result);
						return ;	
			}

			if (file.isFile()&&file.canRead()) {
				result.setSuccess(true);
				result.setMessage(FileIo.readFileAsString(file, "utf-8"));
			} else {
				result.setMessage("文件无法读取！");
			}
		}

		sendResultJson(result);
	}	
	
	
	public void deleteFile() {
		if (StringUtils.isEmpty(editFilePath)) {
			result.setSuccess(false);
			result.setMessage("文件路径为空！");
		} else {
			File file = new File(AutoCodeFileUtil.parseFilePath(editFilePath));
			if (!file.exists()) {
						result.setSuccess(false);
						result.setMessage("文件不存在："+editFilePath);
						sendResultJson(result);
						return ;	
			}

			if (file.isFile()&&file.canWrite()) {
				file.delete();
                result.setSuccess(true);
                result.setMessage("文件删除成功:"+file.getAbsolutePath());
			} else {
				result.setMessage("文件无法删除！");
			}
		}

		sendResultJson(result);
	}	
	
	
	
	
	/**
	 * 打开文件目录
	 */
	public void openDir(){
		if(StringUtils.isEmpty(parentPath)){
			result.setMessage("directory path is null");
		}else{
			String path=AutoCodeFileUtil.parseFilePath(parentPath);
			File file=new File(path);
			if(file.exists()&&file.isDirectory()){
				SystemInvoke.executeWithOutReturn("cmd.exe /c start "+path);
				result.setMessage("open dirctory success ["+path+"]");
				result.setSuccess(true);
			}else{
				result.setMessage("dirctory ["+path+"] is not exists");
			}
		}
		sendResultJson(result);
	}
	
	
	
	/**
	 * 打开文件目录
	 */
	public void listFiles(){
		if(StringUtils.isEmpty(parentPath)){
			result.setMessage("directory path is null");
		}else{
			String path=AutoCodeFileUtil.parseFilePath(parentPath);
			File file=new File(path);
			if(file.exists()&&file.isDirectory()){
				File[] files=file.listFiles(new FileFilter() {
					public boolean accept(File pathname) {
						if(StringUtils.isNotEmpty(fileExt) && pathname.getName().endsWith(fileExt)){
							return true;
						}
						return false;
					}
				});
				
				List<FileInfo> fileList=AutoCodeFileUtil.getFileInfo(files);
				result.addModel("fileList",fileList);
				result.setSuccess(true);
			}else{
				result.setMessage("dirctory ["+path+"] is not exists");
			}
		}
		sendResultJson(result);
	}
	
	
	
public static final String[] allExtensions={"*.*"};

public static final String[] imgExtensions={"*.bmp;*.gif;*.ico;*.jpg;*.pcx;*.png;*.tif", "*.bmp",
	"*.gif", "*.ico", "*.jpg", "*.pcx", "*.png", "*.tif"};

public static final String[] audioExtensions={"*.wav;*.wma;*.rm;*.ram;*.rmvb;*.ra;*.rt;*.mpa;*.mpg;*.mpeg;*.mov;*.3gp;*.mp3",
	"*.wav;*.wma", "*.rm;*.ram;*.rmvb;*.ra;*.rt",
	"*.mpa;*.mpg;*.mpeg", "*.mov;*.3gp", "*.mp3"};

public static final String[] vedioExtensions={"*.avi;*.wmv;*.asf;*.rm;*.rmvb;*.ra;*.rt;*.mpg;*.mpeg;*.mpe;*.vob;*.mp3;*.mov;*.3gp",
	"*.avi;*.wmv;*.asf", "*.rm;*.rmvb;*.ra;*.rt", "*.mpg;*.mpeg;*.mpe",
	"*.vob", "*.mp3", "*.mov;*.3gp"};

public static final String[] docExtensions={"*.txt;*.doc;*.xls;*.ppt","*.txt","*.doc","*.xls","*.ppt"};




	
	public String getDialogType() {
		return dialogType;
	}
	
	public void setDialogType(String dialogType) {
		this.dialogType = dialogType;
	}
	
	public String getFileType() {
		return fileType;
	}
	
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	public String getFileExt() {
		return fileExt;
	}
	
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getEditFilePath() {
		return editFilePath;
	}

	public void setEditFilePath(String editFilePath) {
		this.editFilePath = editFilePath;
	}

	public String getEditFileContent() {
		return editFileContent;
	}

	public void setEditFileContent(String editFileContent) {
		this.editFileContent = editFileContent;
	}

	public String getParentPath() {
		return parentPath;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	public String getChangeCurPath() {
		return changeCurPath;
	}

	public void setChangeCurPath(String changeCurPath) {
		this.changeCurPath = changeCurPath;
	}

}
