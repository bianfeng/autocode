package com.shareyi.joywindow.sys.action;

import org.apache.commons.lang.StringUtils;

import com.shareyi.joywindow.JoyWindowData;
import com.shareyi.joywindow.WindowConfig;
import com.shareyi.joywindow.sys.utils.SystemInvoke;



public class SysUtilAction extends BaseAction{

	private String uri;
	private WindowConfig windowConfig;

	/**
	 * 使用系统地址打开系统资源
	 */
	public void openUri(){
		try{
			if(StringUtils.isEmpty(uri))
			{
				result.setMessage("地址存在问题，无法打开！");
			}
			
			SystemInvoke.executeWithOutReturn("cmd.exe /c start "+uri);
			result.setSuccess(true);
			result.setMessage("成功");
		}catch (Exception e) {
			result.setMessage("打开失败，原因是："+e.getMessage());
		}
		sendResultJson(result);
	}


	/**
	 * 设置窗口大小，透明度
	 */
	public void initWindow(){
		try{
			if(windowConfig==null){
				result.setMessage("parameter is null, set failed！");
			}
			JoyWindowData.getJoyWindow().initWindow(windowConfig);
			result.setSuccess(true);
			result.setMessage("成功");
		}catch (Exception e) {
			result.setMessage("window config failed,the reason is:："+e.getMessage());
		}
		sendResultJson(result);
	}
	
	
	
	public WindowConfig getWindowConfig() {
		return windowConfig;
	}

	public void setWindowConfig(WindowConfig windowConfig) {
		this.windowConfig = windowConfig;
	}
	
	
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
}
