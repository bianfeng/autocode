package com.shareyi.joywindow.sys.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemInvoke {

	public static void executeWithOutReturn(String command) {
		try {
			Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String executeWithReturn(String command) throws IOException {
		
		StringBuilder output = new StringBuilder();
		Runtime run = Runtime.getRuntime();
		Process process = run.exec(command);    
		BufferedReader br= new BufferedReader(new InputStreamReader(process.getInputStream()));
         while(true) {
            String s= br.readLine();
            if (s==null)break;
            output.append(s+"\n");

         }
         return output.toString();
	}

}
