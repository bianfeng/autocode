package com.shareyi.joywindow.sys.utils;

/**
 * Created by david on 2016/6/10.
 */
public class OsUtils {

    /**
     * 是否windows系统
     * @return
     */
    public static boolean isWindows(){
        String os = System.getProperty("os.name");
        return os.toLowerCase().startsWith("win");
    }
}
