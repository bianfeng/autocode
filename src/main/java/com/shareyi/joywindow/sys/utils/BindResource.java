package com.shareyi.joywindow.sys.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Properties;

import com.shareyi.fileutil.FileUtil;
import com.shareyi.joywindow.JoyWindowData;


public abstract class BindResource {

	
	private static HashMap<String,Properties> resourceMap=new HashMap<String, Properties>();
	
	
	/**
	 * 获取配置资源
	 * @param bindId
	 * @return
	 */
	public static Properties getBindProperties(String bindId)
	{
		Properties pro= resourceMap.get(bindId);
		if(pro==null)
		{
			String bindSrc=FileUtil.contactPath(JoyWindowData.getHttpServer().getBindPath(),bindId+".properties");
			File file=new File(bindSrc);
			if(file.exists()&&file.isFile()&&file.canRead())
			{
				pro=new Properties();
				try {
					pro.load(new FileInputStream(file));
				} catch (Exception e) {
				}
			}
			
		 resourceMap.put(bindId, pro);
		}
		return pro;
	}


	/**
	 * 存储配置文件
	 * @param bindId
	 */
	public static  boolean saveBindProperties(String bindId,Properties pro) {
			if(pro==null)
				{
					pro=resourceMap.get(bindId);
				}else
				{
					resourceMap.put(bindId, pro);
				}
			
			if(pro!=null){
				String bindSrc=FileUtil.contactPath(JoyWindowData.getHttpServer().getBindPath(),bindId+".properties");
				File file=new File(bindSrc);
				FileUtil.makeSureFileExsit(file);
				if(file.canWrite()){
					try {
							pro.store(new FileOutputStream(file), "update config file");
						} catch (Exception e) {
								return false;
						}
				}else{
						return false;
				}

			}else return false;
			
			
			
			return true;
	}



	/**
	 * 获取配置资源
	 * @param bindId
	 * @return
	 */
	public static Properties getProperties(String bindId)
	{
		Properties pro= null;
		String bindSrc=FileUtil.contactPath(FileUtil.getRunPath(),bindId+".properties");
		File file=new File(bindSrc);
		if(file.exists()&&file.isFile()&&file.canRead())
		{
			pro=new Properties();
			try {
				pro.load(new FileInputStream(file));
			} catch (Exception e) {
			}
		}
		return pro;
	}


}
