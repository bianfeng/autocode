package com.shareyi.joywindow.sys.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Result<T> implements Serializable{
	private static final long serialVersionUID = 4067914314823897037L;
	
	/**
	 * 是否成功
	 */
	private boolean success = false;
	
	
	/**
	 * 结果消息
	 */
	private String message;
	
	
	/**
	 * service返回的对象
	 */
	private Map<String, Object> result = new HashMap<String, Object>();

	
	/**
	 * 带是否成功的构造方法
	 * 
	 * @param success
	 */
	public Result(boolean success) {
		this.success = success;
	}


	/**
	 * 默认构造方法
	 */
	public Result() {
	}

	

	/**
	 * 新增一个带key的返回结果
	 * 
	 * @param key
	 * @param obj
	 * @return
	 */
	public Object addModel(String key, Object obj) {
		return result.put(key, obj);
	}

	/**
	 * 取出所有的key
	 * 
	 * @return
	 */
	public Set<String> keySet() {
		return result.keySet();
	}


	/**
	 * 取出值
	 * 
	 * @param key
	 * @return
	 */
	public Object get(String key) {
		return result.get(key);
	}
	
	public int getInt(String key) {
		String obj = this.getString(key);
		return Integer.parseInt(obj);
	}
	
	public String getString(String key) {
		Object obj = result.get(key);
        if(obj == null) return null;
        return obj.toString();
	}

	/**
	 * 取出值集合
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Collection values() {
		return result.values();
	}

	/**
	 * 返回是否成功
	 * 
	 * @return
	 */
	public boolean getSuccess() {
		return success;
	}

	public boolean isSuccess() {
		return success;
	}
	
	/**
	 * 将错误信息键值对设置到Result中
	 * 
	 * @param errorMessages
	 */
	public void setErrorMessages(Map<String, String> errorMessages) {
		this.addModel("errorMessages", errorMessages);
	}

	/**
	 * 设置返回是否成功
	 * 
	 * @param success
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	

	public Result setResultInfo(boolean success,String message){
		this.success=success;
		this.message=message;
		return this;
	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Result append(Result result) {
		this.getResult().putAll(result.getResult());
		return this;
	}




	
	
}
