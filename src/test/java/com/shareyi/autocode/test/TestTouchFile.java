package com.shareyi.autocode.test;

import com.shareyi.jshop.autocode.domain.TouchFile;
import com.shareyi.jshop.autocode.domain.TouchFileDir;
import com.shareyi.jshop.autocode.service.CopyAndReplaceService;
import com.shareyi.jshop.autocode.service.FileNameFilter;
import com.shareyi.jshop.autocode.utils.TouchFileHelper;
import junit.framework.Assert;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;

import java.io.File;

/**
 * Created by david on 2016/1/3.
 */
public class TestTouchFile {

    String dirReplaceExp="fir/sec=newdir\nthr/fourth=mydir";

    @Test
    public void  testTouchFileParse(){
        TouchFile touchFile=TouchFile.parseExpression("jd/ka=shareyi");
        System.out.println(touchFile.getOriginPathList());
        System.out.println(touchFile.getTouchFileType());
        System.out.println(touchFile.getDirTransList());
        System.out.println(touchFile);
        System.out.println();

        TouchFile touchFile2=TouchFile.parseExpression("com/jd/ka=com/shareyi/myprj/soso");
        System.out.println(touchFile2.getOriginPathList());
        System.out.println(touchFile2.getTouchFileType());
        System.out.println(touchFile2.getDirTransList());
        System.out.println(touchFile2);

        TouchFile touchFile3=TouchFile.parseExpression("shareyi/baseprj=shareyi/baseprj/subprj");
        System.out.println(touchFile3.getOriginPathList());
        System.out.println(touchFile3.getTouchFileType());
        System.out.println(touchFile3.getDirTransList());
        System.out.println(touchFile3);
    }


    @Test
    public void  testGetTouchFileHelper(){
        TouchFileHelper touchFileHelper=TouchFileHelper.getTouchFileHelper(dirReplaceExp);
        Assert.assertNotNull(touchFileHelper);
    }



    @Test
    public void  testParseTouchFileResult2(){
        String twoReplaceReg="fir/sec=newdir\n" + "thr/fourth=mydir";
        TouchFileHelper touchFileHelper=TouchFileHelper.getTouchFileHelper(twoReplaceReg);
        File rootFile=new File("i:/test");
        FileNameFilter fileNameFilter=new FileNameFilter() {
            @Override
            public boolean isMatch(String name) {
                return false;
            }
        };
        TouchFileDir rootDir=CopyAndReplaceService.parseTouchFileResult(rootFile, touchFileHelper, null, fileNameFilter, fileNameFilter);
        Assert.assertNotNull(rootDir);

        TouchFileDir firDir=rootDir.getChildTouchFileDirByDirName("fir");
        Assert.assertNotNull(firDir);
        Assert.assertNotNull(firDir.getTouchFileResult());
        Assert.assertEquals(firDir.getTouchFileResult().dirLevel, firDir.getLevel());


        TouchFileDir secDir=firDir.getChildTouchFileDirByDirName("sec");
        Assert.assertEquals(firDir.getTouchFileResult(),secDir.getTouchFileResult());


        TouchFileDir thrDir=secDir.getChildTouchFileDirByDirName("thr");
        Assert.assertNotNull(thrDir);
        Assert.assertNotNull(thrDir.getTouchFileResult());


        TouchFileDir fourthDir=thrDir.getChildTouchFileDirByDirName("fourth");
        Assert.assertNotNull(fourthDir);
        Assert.assertEquals(thrDir.getTouchFileResult(), fourthDir.getTouchFileResult());
    }



    @Test
    public void  testParseTouchFileResult(){
        String twoReplaceReg="fir/sec=fir/newsec\n" + "sec/thr=sec/newthr";
        TouchFileHelper touchFileHelper=TouchFileHelper.getTouchFileHelper(twoReplaceReg);
        File rootFile=new File("i:/test");
        FileNameFilter fileNameFilter=new FileNameFilter() {
            @Override
            public boolean isMatch(String name) {
                return false;
            }
        };
        TouchFileDir rootDir=CopyAndReplaceService.parseTouchFileResult(rootFile, touchFileHelper, null, fileNameFilter, fileNameFilter);
        Assert.assertNotNull(rootDir);

        TouchFileDir firDir=rootDir.getChildTouchFileDirByDirName("fir");
        Assert.assertNotNull(firDir);
        Assert.assertNull(firDir.getTouchFileResult());

        TouchFileDir secDir=firDir.getChildTouchFileDirByDirName("sec");
        Assert.assertNotNull(secDir.getTouchFileResult());


        TouchFileDir thrDir=secDir.getChildTouchFileDirByDirName("thr");
        Assert.assertNotNull(thrDir);
        Assert.assertNotNull(thrDir.getTouchFileResult());


      //  TouchFileDir fourthDir=thrDir.getChildTouchFileDirByDirName("fourth");
       // Assert.assertNotNull(fourthDir);
      //  Assert.assertEquals(thrDir.getTouchFileResult(), fourthDir.getTouchFileResult());
    }


    @Test
    public void testSplit(){
        String[] strings="good\r\nhello\ngood\rhello".split("\r\n");
        for(String s:strings){
            System.out.println(s);
        }
    }
}
